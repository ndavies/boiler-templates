from flaskext.script import Manager
from flask_application import app

manager = Manager(app)

@manager.command
def runserver():
    if app.debug:
        app.run(debug=True)
    else:
        app.run(host='0.0.0.0')

# BB:START(flask_manage_py)
# BB:END

if __name__ == "__main__":
    manager.run()
